Controles:

- Boton A: Play/Pause
- Boton B: Mute
- Start: Siguiente canción
- Select: Anterior canción
- Cruceta derecha: Adelantar canción
- Cruceta izquierda: Atrasar canción
- Cruceta arriba: Subir volumen
- Cruceta abajo: Bajar volumen