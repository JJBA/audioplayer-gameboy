class AudioPlayer{
    constructor(src, post, currentS, controls){
        this.currentS=currentS;
        this.post=post;
        this.src=src;
        this.audio=new Audio(this.src);
        this.controls = controls;
        this.initControls();

        this.audio.ontimeupdate = () => { this.unit(); }
    }


    initControls(){
        this.play();
        this.initPlay(this.controls.play);
        this.initStop(this.controls.stop);
        this.initAdvance(this.controls.advance);
        this.initDelay(this.controls.delay);
        this.initBack(this.controls.back);
        this.initPass(this.controls.pass);
        this.initDecrea(this.controls.decrease);
        this.initIncrea(this.controls.increase);
    }

    initPlay(domElement){
        domElement.onclick = () => {
            this.playOrPause();
        }
    }

    initStop(domElement){
        domElement.onclick = () => {
            this.stop();
        }
    }

    initAdvance(domElement){
        domElement.onclick = () => {
            this.advance();
        }
    }

    initDelay(domElement){
        domElement.onclick = () => {
            this.delay();
        }
    }

    initBack(domElement){
        domElement.onclick = () => {
            this.back();
        }
    }

    initPass(domElement){
        domElement.onclick = () => {
            this.pass();
        }
    }

    initDecrea(domElement){
        domElement.onclick = () => {
            this.decrease();
        }
    }

    initIncrea(domElement){
        domElement.onclick = () => {
            this.increase();
        }
    }

    play(){
        var songTitle = document.getElementById("name");
        this.audio.src=this.src[this.currentS];
        songTitle.textContent = this.src[this.currentS];
        this.audio.volume=0.60;
        this.audio.play();
    }

    stop(){
        if(this.audio.volume==0){
            this.audio.volume=0.4;
        }else{
            this.audio.volume=0;
        }
    }

    playOrPause(){
        if(this.audio.paused){
            this.audio.play();
        }
        else{
            this.audio.pause();
        }
    }

    unit(){
        var fillbar = document.getElementById("progress");
        var position = this.audio.currentTime / this.audio.duration;

        fillbar.style.width = position * 100 + '%';

        this.convertTime(Math.round(this.audio.currentTime));
    }

    convertTime(seconds){

        var min = Math.floor(seconds / 60);
        var sec = seconds % 60;

        min = (min < 10) ? "0" + min : min;
        sec = (sec < 10) ? "0" + sec : sec;
        currentTime.textContent = min + ":" + sec;

        this.totalTime(Math.round(this.audio.duration));
    }

    totalTime(seconds){
        var min = Math.floor(seconds / 60);
        var sec = seconds % 60;

        min = (min < 10) ? "0" + min : min;
        sec = (sec < 10) ? "0" + sec : sec;
        currentTime.textContent += "/" + min + ":" + sec;
    }
    

    pass(){
        var img = document.getElementById("img")
        this.currentS++;
        if(this.currentS > 2){
            this.currentS=0;
        }
        this.play();
        img.src=this.post[this.currentS];
    }

    back(){
        var img = document.getElementById("img")
        this.currentS--;
        if(this.currentS < 0){
            this.currentS = 2;
        }
        this.play();
        img.src=this.post[this.currentS];
    }

    delay(){
        this.audio.currentTime -= 5;
    }

    advance(){
        this.audio.currentTime += 5;
    }

    decrease(){
        this.audio.volume -= 0.10;
    }

    increase(){
        this.audio.volume += 0.10;
    }
}