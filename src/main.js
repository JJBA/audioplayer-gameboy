const playBtn = document.querySelector(".container .playBtn");
const pauseBtn = document.querySelector(".container .pauseBtn");
const advanceBtn = document.querySelector(".container .cruceta .after");
const delayBtn = document.querySelector(".container .cruceta .before");
const backBtn = document.querySelector(".container .backBtn");
const passBtn = document.querySelector(".container .passBtn");
const decreaBtn = document.querySelector(".container .cruceta .decrease");
const increaBtn = document.querySelector(".container .cruceta .increase");

const controls = {
    play: playBtn,
    stop: pauseBtn,
    advance: advanceBtn,
    delay: delayBtn,
    back: backBtn,
    pass: passBtn,
    decrease: decreaBtn,
    increase: increaBtn
}
const songs = ["./assets/music/Sono Chi no Sadame.mp3", "./assets/music/A cruel angel's tesis.mp3", "./assets/music/Guren no Yumiya.mp3"];
const post = ["./assets/pics/1.jpg", "./assets/pics/2.jpg", "./assets/pics/3.jpg"];
const currentSong = 0;
const player = new AudioPlayer(songs, post, currentSong, controls);